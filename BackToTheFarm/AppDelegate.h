//
//  AppDelegate.h
//  BackToTheFarm
//
//  Created by Siddhant Dange on 11/27/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScreenNavigation.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *mainWindow;
@property (nonatomic, strong) ScreenNavigation *screenNavigation;


@end

