//
//  AppDelegate.m
//  BackToTheFarm
//
//  Created by Siddhant Dange on 11/27/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "AppDelegate.h"
#import "User.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    //make main navigation
    _mainWindow = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _screenNavigation = [[ScreenNavigation alloc] init];
    [_screenNavigation startWithScreen:@"MainScreen" data:nil animated:NO];
    _mainWindow.rootViewController = _screenNavigation;
    [_mainWindow addSubview:_screenNavigation.view];
    [_mainWindow makeKeyAndVisible];
    
    User *user = [User sharedInstance];
    user.meatData = @{
                @"cow" : @{
                        @"Forequarter" : @{
                              @"Shoulder" : @{
                                      @"Chuck Eye" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Chuck Roll" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Flat Iron" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Palomilla" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Petite Fillet" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Back Ribs" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Cross Ribs Steak" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Brisket" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                              }.mutableCopy,
                              
                              @"Ribs" : @{
                                      
                                      @"Ribeye" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Short Ribs" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Outside Skirt" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                              }.mutableCopy,
                        }.mutableCopy,
                        @"Hindquarter" : @{
                              @"Loin" : @{
                                      @"Bavette" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Flank" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"New York (Dry/BI)" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Sirloin Tip" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Tenderloin (Dry)" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Top Sirloin" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Tritip" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      }.mutableCopy,
                              @"Round" : @{
                                      @"Baseball Steak" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Bottom Round" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Carne Asada" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Eye of Round" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Oyster" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Picanha" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Poire" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Top Round" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Velvet Steak" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      @"Stew Meat" : @{
                                              @"Meat" : @(40),
                                              @"Trim" : @(30),
                                              @"Dogfood" : @(20),
                                              @"Bones" : @(5),
                                              @"Waste" : @(5),
                                              }.mutableCopy,
                                      }.mutableCopy,
                        }.mutableCopy,
                }.mutableCopy, //cow
                
                @"pig" : @{
                        
                        }.mutableCopy,
                @"sheep" : @{
                        
                        }.mutableCopy,
                @"goat" : @{
                        
                        }.mutableCopy
            }.mutableCopy;
    
    [user loadMeatFromFile];
    
    return YES;
}

-(float)generateTotal:(NSDictionary*)meats{
    float total = 0.0f;
    
    if([meats objectForKey:@"Meat"]){
        for(NSString *key in meats){
            total += ((NSNumber*)meats[key]).floatValue;
        }
        
        return total;
    }
    
    for(NSString* key in meats){
        total += [self generateTotal:meats[key]];
    }
    
    return total;
}

-(NSMutableDictionary*)generatePercData:(NSMutableDictionary*)meats{
    float totalWeight = [self generateTotal:meats];
    NSMutableDictionary *percData = (NSMutableDictionary*)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFPropertyListRef)meats, kCFPropertyListMutableContainers));
    [self meatsToPerc:percData withTotalWeight:totalWeight];
    return percData;
}


-(void)meatsToPerc:(NSMutableDictionary*)meats withTotalWeight:(float)totalWeight{
    for(NSString *key in meats.allKeys){
        if([meats[key] isKindOfClass:[NSDictionary class]]){
            [self meatsToPerc:meats[key] withTotalWeight:totalWeight];
        } else{
            NSNumber *val = meats[key];
            float perc = val.floatValue / totalWeight;
            [meats setObject:@(perc) forKey:key];
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
